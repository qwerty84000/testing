using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace KvartPlata
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //обработчики чекбоксов, показывают/скрывают соответствующие элементы
        private void checkBoxBElectro_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownElectro.Enabled = checkBoxBElectro.Checked;
        }

        private void checkBoxElectro_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxBElectro.Enabled = checkBoxElectro.Checked;            
        }

        private void checkBoxBHot_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownHot.Enabled = checkBoxBHot.Checked;
        }

        private void checkBoxHot_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxBHot.Enabled = checkBoxHot.Checked;            
        }

        private void checkBoxBCold_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownCold.Enabled = checkBoxBCold.Checked;
        }

        private void checkBoxCold_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxBCold.Enabled = checkBoxCold.Checked;            
        }

        private void checkBoxBGas_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownGas.Enabled = checkBoxBGas.Checked;
        }

        private void checkBoxGas_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxBGas.Enabled = checkBoxGas.Checked;            
        }

        private void checkBoxBHeat_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownHeat.Enabled = checkBoxBHeat.Checked;
        }

        private void checkBoxHeat_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxBHeat.Enabled = checkBoxHeat.Checked;            
        }

        //кнопка расчета
        private void buttonCalc_Click(object sender, EventArgs e)
        {
            String text = ««;
            float sum = 0;

            //класс выполняющий расчеты
            Values val = new Values();

            //вызываем функцию, передавая в нее введенные пользователем значения
            sum = val.getCost((int)numericUpDownS.Value, (int)numericUpDownPeople.Value, (int)numericUpDownFloor.Value,
                checkBoxCap.Checked, checkBoxClean.Checked, checkBoxLift.Checked,
                checkBoxElectro.Checked, (int)numericUpDownElectro.Value, checkBoxBElectro.Checked,
                checkBoxHot.Checked, (int)numericUpDownHot.Value, checkBoxBHot.Checked,
                checkBoxCold.Checked, (int)numericUpDownCold.Value, checkBoxBCold.Checked,
                checkBoxGas.Checked, (int)numericUpDownGas.Value, checkBoxBGas.Checked,
                checkBoxHeat.Checked, (int)numericUpDownHeat.Value, checkBoxBHeat.Checked,
                out text);

            //выводим результат
            textBoxResult.Text = text;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            //saveFileDialog.InitialDirectory = GO.AppFolders.Temporary;
            saveFileDialog.Filter = «Текстовые файлы|*.txt»;
            saveFileDialog.DefaultExt = «.txt»;
            saveFileDialog.AddExtension = true;
            saveFileDialog.ShowDialog();

            string ReadResFilePlace = saveFileDialog.FileName; //считываем путь к файлу с результатом

            if (ReadResFilePlace != String.Empty)
            {
                using (StreamWriter sw = new StreamWriter(ReadResFilePlace))
                {
                    sw.WriteLine(textBoxResult.Text);
                }
            }
          
        }
