using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KvartPlata
{
    //класс для расчета 
    class Values
    {
        float costS = 380.0f;   //цена за кв метр

        float costCap = 50.0f;  //добавка к цене за кв метр при капитальном ремонте

        float costClean = 200.0f;   //стоимость уборки подъезда

        float costLift = 214.0f;    //цена за лифт
        float liftK = 0.1f;         //наценка за лифт в зависимости от этажа

        float costElectro = 2.01f;      //цена киловатта без газовой плиты
        float costElectroGas = 2.36f;       //цена киловатта с исп газовой плиты
        float [] electroNorm = new float[4] { 90, 56, 43, 35 }; //нормативы потребления эл на человека в зависимости от кол-ва людей

        float costHot = 34.02f; //цена горячей воды
        float hotNorm = 3.81f;  //норматив потребления горячей воды

        float costCold = 21.03f;    //цена за холодную воду
        float coldNorm = 9.29f;     //норматив потребления холодной воды

        float costGas = 52.4f;  //цена за газ
        float gasNorm = 15;     //норматив потребления газа

        float costHeat = 1408;  //цена за отопление
        float heatNorm = 0.022f;    //норматив потребления

        //функция расчета
        public float getCost(int s,     //площадь
                int people,             //кол-во людей
                int floor,              //этаж
                bool capital,           //наличие кап ремонта
                bool clean,             //уборка подъездов
                bool lift,              //наличие лифта
                bool isEl,              //оплата электричества
                int elNum,              //расход электричества
                bool isElC,             //электричество по счетчику     
                bool isHot,             //оплата гор воды
                int hotNum,             //расход гор воды
                bool isHotC,            //гор вода по счетчику
                bool isCold,            //оплата хол воды
                int coldNum,            //расход хол воды
                bool isColdC,           //хол вода по счетчику
                bool isGas,             //оплата газа
                int gasNum,             //расход газа
                bool isGasC,            //газ по счетчику
                bool isHeat,            //оплата отопления
                int heatNum,            //расход 
                bool isHeatC,           //учет отопления
            out string text             //выходной параметр - отчет
            )
        {
            float sum;
            text = ««;

            sum = s * costS;
            text += «Цена за квадратный метр=«+costS+», за всю площадь=«+(s * costS);

            if (capital)
            {
                sum += s * costCap;
                text += «\r\nКапитальный ремонт добавляет за каждый квадратный метр=« + costCap + «, за всю площадь=« + (s * costCap);
            }

            if (clean)
            {
                sum += costClean;
                text += «\r\nЦена за уборку подъездов=« + costClean;

            }

            if (lift)
            {
                sum += costLift * (1 + floor * liftK);
                text += «\r\nЦена за лифт=« + costLift + «, за каждый этаж добавляется коэфициент=« + liftK + «, итого=« + (costLift * (1 + floor * liftK));

            }

            if (isEl)
            {
                if (isElC)
                {
                    if (isGas)
                    {
                        sum += elNum * costElectroGas;
                        text += «\r\nЦена за электричество при наличии газовой плиты и счетчика=« + costElectroGas + «, итого=« + (elNum * costElectroGas);

                    }
                    else
                    {
                        sum += elNum * costElectro;
                        text += «\r\nЦена за электричество без газовой плиты и с счетчиком=« + costElectro + «, итого=« + (elNum * costElectro);
                    }
                }
                else
                {
                    int peopleI = people;
                    string str;

                    //если людей больше чем эл массива
                    if (peopleI >= electroNorm.Length)
                    {
                        //берем последний элемент
                        peopleI = electroNorm.Length - 1;
                        str = «от « + peopleI + « человек»;
                    }
                    else
                        str = peopleI + « человек»;

                    float electroNormAll = electroNorm[peopleI] * peopleI;       

                    if (isGas)
                    {
                        sum += electroNormAll * costElectroGas;
                        text += «\r\nНорматив за электричество с газовой плитой при проживании « + str + « « + electroNorm[peopleI]
                            + «, итого=« + (electroNormAll * costElectroGas);
                    }
                    else
                    {
                        sum += electroNormAll * costElectro;
                        text += «\r\nНорматив за электричество без газовой плитой при проживании « + str + « « + electroNorm[peopleI]
                            + «, итого=« + (electroNormAll * costElectro);
                    }
                }
            }

            if (isHot)
            {
                if (isHotC)
                {
                    sum += costHot * hotNum;
                    text += «\r\nЦена за горячую воду при наличии счетчика=« + costHot + «, итого=« + (costHot * hotNum);
                }
                else
                {
                    sum += costHot * hotNorm;
                    text += «\r\nЦена за горячую воду без счетчика=« + costHot + «, норматив=« + hotNorm + «, итого=« + (costHot * hotNum);
                }
            }

            if (isCold)
            {
                if (isColdC)
                {
                    sum += costCold * coldNum;
                    text += «\r\nЦена за холодную воду при наличии счетчика=« + costCold + «, итого=« + (costCold * coldNum);
                }
                else
                {
                    sum += costCold * coldNorm;
                    text += «\r\nЦена за холодную воду без счетчика=« + costCold + «, норматив=« + coldNorm + «, итого=« + (costCold * coldNorm);
                }
            }

            if (isGas)
            {
                if (isGasC)
                {
                    sum += costGas * gasNum;
                    text += «\r\nЦена за газ при наличии счетчика=« + costGas + «, итого=« + (costGas * gasNum);
                }
                else
                {
                    sum += costGas * gasNorm;
                    text += «\r\nЦена за газ без счетчика=« + costGas + «, норматив=« + gasNorm + «, итого=« + (costGas * gasNorm);

                }
            }

            if (isHeat)
            {
                if (isHeatC)
                {
                    sum += costHeat * heatNum/1000.0f;
                    text += «\r\nЦена за отопление при наличии счетчика=« + costHeat + «, итого=« + (costHeat * heatNum);
                }
                else
                {
                    sum += costHeat * heatNorm;
                    text += «\r\nЦена за отопление=« + costHeat + «, норматив=« + heatNorm + «, итого=« + (costHeat * heatNorm);
                }
            }

            text += «\r\n------------\r\nИтого:» + sum;

            return sum;
        }
    }
}
